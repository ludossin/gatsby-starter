const path = require(`path`)

// exports.createSchemaCustomization = ({ actions }) => {
//   const { createTypes } = actions
//   const typeDefs = `
//     type Item implements Node @dontInfer {
//       title: String!
//     }
//   `
//   createTypes(typeDefs)
// }

exports.createPages = async function ({ actions, graphql }) {
  const { data } = await graphql(`
    query {
      allWpItem {
        nodes {
          slug
          link
          title
          featuredImage {
            node {
              altText
              caption
              mediaItemUrl
            }
          }
        }
      }
    }
  `)
  data.allWpItem.nodes.forEach(node => {
    const slug = node.slug
    actions.createPage({
      path: `items/${slug}`,
      component: require.resolve(`./src/templates/item.js`),
      context: { slug: slug },
    })
  })
}