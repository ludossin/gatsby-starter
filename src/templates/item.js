import * as React from "react"
import { Link, graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import Header from "../components/header"
import Layout from "../components/layout"
import PageTransition from "gatsby-plugin-page-transitions"

const Item = ({data}) => {

	const post = data.wpItem;

	return (
	<PageTransition>
    <Header />
    <main>
      <Layout>
      <div className="column is-8">
		    <h1>{post.title}</h1>
		    <GatsbyImage
          image={getImage(post.featuredImage.node.localFile)}
          alt={post.title}
        />
      </div>

      <div className="column is-8">
       <div
          dangerouslySetInnerHTML={{ __html: post.content }}
        />

        {post.pageLink.acfPageLink && (
          <>
            <h2>Related content</h2>
            <Link to={post.pageLink.acfPageLink.link}>{post.pageLink.acfPageLink.title}</Link>
          </>
          )
        }
      </div>
      </Layout>
    </main>   

	</PageTransition>
	)

}

export default Item

export const Head = () => <title>Items Page</title>

export const query = graphql`
 query($slug: String!) {
 	wpItem(slug: { eq: $slug }) {
    content
    title
    pageLink {
      acfPageLink {
        ... on WpPost {
          id
          slug
          link
          title
        }
        ... on WpPage {
          id
          slug
          link
          title
        }
        ... on WpItem {
          id
          slug
          link
          title
        }
      }
    }
    featuredImage {
      node {
        altText
        caption
        localFile {
          childImageSharp {
            gatsbyImageData(
            layout: CONSTRAINED
            )
            id
            original {
              src
            }
          }
        }
      }
    }
 	}
 }

`