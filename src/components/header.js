import React from "react"
import MainNav from "./mainNav"

export default function Layout({ children }) {
  return (
    <div className="container">
      <div className="columns is-centered">
          <div className="column is-8">
            <MainNav />
          </div>
      </div>
    </div>
  )
}