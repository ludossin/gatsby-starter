/**
 * Creates hierarchical menu based on WordPress menu.
 * @link https://www.wpgraphql.com/docs/menus/#hierarchical-data
 */
import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import UniversalLink from "../utils/universalLink"
import { FlatListToHierarchical } from "../utils/flatListToHierarchical"

const MenuLoop = ({ menuItems }) => {
  return (
    <>
    <ul className="navbar-menu" id="navMenu">
      {menuItems.map((menuItem, index) => {
        return (
          <li
            key={index}
            className={menuItem.routes.length > 0 ? "has-submenu" : undefined}
          >
            <UniversalLink to={menuItem.path} activeClassName="current-page">
              {menuItem.title}
            </UniversalLink>
            {menuItem.routes.length > 0 && (
              <MenuLoop menuItems={menuItem.routes}></MenuLoop>
            )}
          </li>
        )
      })}
    </ul>
    <a role="button" className="navbar-burger" data-target="navMobile" aria-label="menu" aria-expanded="false">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
    <ul className="navbar-mobile is-hidden-desktop" id="navMobile">
      {menuItems.map((menuItem, index) => {
        return (
          <li
            key={index}
            className={menuItem.routes.length > 0 ? "has-submenu" : undefined}
          >
            <UniversalLink to={menuItem.path} activeClassName="current-page">
              {menuItem.title}
            </UniversalLink>
            {menuItem.routes.length > 0 && (
              <MenuLoop menuItems={menuItem.routes}></MenuLoop>
            )}
          </li>
        )
      })}
    </ul>
    </>
  )
}

const MainNav = () => {
  const wpMenu = useStaticQuery(graphql`
    {
      wpMenu(slug: { eq: "primary-menu" }) {
        menuItems {
          nodes {
            id
            title: label
            path
            target
            parent: parentId
          }
        }
      }
    }
  `)

  console.log("Raw data:", wpMenu.wpMenu.menuItems.nodes)
  const headerMenu = FlatListToHierarchical(wpMenu.wpMenu.menuItems.nodes, {
    idKey: "id",
    childrenKey: "routes",
    parentKey: "parent",
  })
  console.log("headerMenu: ", headerMenu)

  return (
    <nav aria-label="Main">
      {headerMenu.length > 0 && <MenuLoop menuItems={headerMenu}></MenuLoop>}
    </nav>
  )
}

export default MainNav
