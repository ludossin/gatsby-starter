import React from "react"

export default function Layout({ children }) {
  return (
    <div className="container">
      <div className="columns is-multiline is-centered">
      {children}
      </div>
    </div>
  )
}