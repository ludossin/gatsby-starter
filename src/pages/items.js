import * as React from "react"
import { Link, graphql } from "gatsby"
import Header from "../components/header"
import Layout from "../components/layout"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import PageTransition from "gatsby-plugin-page-transitions"

const Items = ({ data }) => {

  const posts = data.allWpItem.nodes;

  return (
    <PageTransition
      defaultStyle={{
      transition: 'left 500ms cubic-bezier(0.47, 0, 0.75, 0.72)',
      left: '100%',
      position: 'absolute',
      width: '100%',
    }}
    transitionStyles={{
      entering: { left: '0%' },
      entered: { left: '0%' },
      exiting: { left: '100%' },
    }}
    transitionTime={500}
    >
      <Header />
      <main>
        <Layout>
          <p>All Items List.</p>

          {posts.map((post, index) => (
           <div data-sal="slide-up" className="row" key={`${post.id}`}>
           <Link to={`/items/${post.slug}`} title={`/items/${post.slug}`}>
            <GatsbyImage
              image={getImage(post.featuredImage.node.localFile)}
              alt={post.title}
            />
           {post.title}
           </Link>
           </div>
          ))}
        </Layout>
      </main>

    </PageTransition>
    )
}

export default Items

export const Head = () => <title>Items Page</title>

export const query = graphql`
query {
  allWpItem {
    nodes {
      slug
      link
      title
      featuredImage {
        node {
          altText
          caption
          localFile {
            childImageSharp {
              gatsbyImageData(
              layout: CONSTRAINED
              )
              id
              original {
                src
              }
            }
          }
        }
      }
    }
  }
}
`