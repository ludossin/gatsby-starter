import * as React from "react"
import { Link, graphql } from "gatsby"
import Header from "../components/header"
import Layout from "../components/layout"

const About = ({ data }) => {

  const page = data.wpPage

  return (
    <>
    <Header />
    <main>
      <Layout>
       <div className="column is-8">
        <div
          dangerouslySetInnerHTML={{ __html: page.content }}
        />
       </div>

        <div className="column is-8">
        {page.pageLink.acfPageLink && (
          <>
            <h2>Related Content</h2>
            <Link to={page.pageLink.acfPageLink.link}>{page.pageLink.acfPageLink.title}</Link>
          </>
          )
        }
       </div>
      </Layout>
    </main>
    </>
    )
  }

export default About

export const Head = () => <title>About Page</title>

export const query = graphql`
query {
  wpPage(slug: {eq: "about"}) {
    id
    link
    content
    title
    pageLink {
      acfPageLink {
        ... on WpPost {
          id
          slug
          link
          title
        }
        ... on WpPage {
          id
          slug
          link
          title
        }
        ... on WpItem {
          id
          slug
          link
          title
        }
      }
    }
  }
}
`